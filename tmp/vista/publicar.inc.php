			<div class="main">
				<!-- Á -->
				<div class="main_publicar_titulo_principal">
					<p class="main_publicar_titulo_principal">
						<span class="main_publicar_titulo_principal">Publicar</span>
					</p>
				</div>
				<?php
					if (count($errores) > 0) {
				?>
						<div class="main_publicar_errores">
				<?php
							foreach ($errores as $error) {
				?>
								<p class="main_publicar_errores"><span class="main_publicar_errores"><?= htmlspecialchars($mensajes_error[$error], ENT_COMPAT, _SITIO_CHARSET) ?></span></p>
				<?php
							}
				?>
						</div>
				<?php
					}
				?>
				<div class="main_publicar_formulario">
					<form method=post enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="<?= _SITIO_MAX_FILE_SIZE ?>" />
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Título:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="text" class="main_publicar_formulario_fila_campo" tabindex=1 name="form_titulo" autocomplete="on" autofocus="autofocus" placeholder="Título del aviso" maxlength="200" value="<?= htmlspecialchars($_POST["form_titulo"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Duración (días):</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=2 name="form_duracion">
										<?php
											foreach ($opciones_duracion as $duracion_dias => $duracion_texto) {
												$seleccionada = ($_POST["form_duracion"] == $duracion_dias) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $duracion_dias ?>" <?= $seleccionada ?>><?= htmlspecialchars($duracion_texto, ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">Dirección:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="text" class="main_publicar_formulario_fila_campo" tabindex=3 name="form_direccion" autocomplete="on" placeholder="Dirección del aviso" maxlength="70" value="<?= htmlspecialchars($_POST["form_direccion"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Provincia/localidad:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=4 name="form_localidad">
										<?php
											foreach ($localidades as $localidad) {
												$seleccionada = ($_POST["form_localidad"] == $localidad["codigo"]) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $localidad["codigo"] ?>" <?= $seleccionada ?>><?= htmlspecialchars($localidad["provincia"] . " => " . $localidad["localidad"], ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila_textarea">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Descripción:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo_textarea">
								<p>
									<textarea class="main_publicar_formulario_fila_campo" tabindex=5 name="form_descripcion" maxlength="65535" placeholder="Descripción del aviso"><?= htmlspecialchars($_POST["form_descripcion"], ENT_COMPAT, _SITIO_CHARSET) ?></textarea>
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Mostrar mail:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=6 name="form_mostrar_mail">
										<?php
											foreach ($si_no as $codigo_si_no => $texto_si_no) {
												$seleccionada = ($_POST["form_mostrar_mail"] == $codigo_si_no) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $codigo_si_no ?>" <?= $seleccionada ?>><?= htmlspecialchars($texto_si_no, ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Oferta:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=7 name="form_oferta">
										<?php
											foreach ($ofertas as $codigo_oferta => $texto_oferta) {
												$seleccionada = ($_POST["form_oferta"] == $codigo_oferta) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $codigo_oferta ?>" <?= $seleccionada ?>><?= htmlspecialchars($texto_oferta, ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Marca/modelo:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=8 name="form_modelo">
										<?php
											foreach ($modelos_autos as $modelo) {
												$seleccionada = ($_POST["form_modelo"] == $modelo["codigo"]) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $modelo["codigo"] ?>" <?= $seleccionada ?>><?= htmlspecialchars($modelo["marca"] . " => " . $modelo["modelo"], ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Año:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=9 name="form_anno">
										<?php
											foreach ($annos_autos as $anno_auto) {
												$seleccionada = ($_POST["form_anno"] == $anno_auto) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $anno_auto ?>" <?= $seleccionada ?>><?= htmlspecialchars($anno_auto, ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Combustible:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=10 name="form_combustible">
										<?php
											foreach ($combustibles as $combustible) {
												$seleccionada = ($_POST["form_combustible"] == $combustible["codigo"]) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $combustible["codigo"] ?>" <?= $seleccionada ?>><?= htmlspecialchars($combustible["tipo"], ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Kilometraje (sin separador de miles):</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="text" class="main_publicar_formulario_fila_campo" tabindex=11 name="form_kilometraje" autocomplete="on" placeholder="Kilometraje del auto" maxlength="13" value="<?= htmlspecialchars($_POST["form_kilometraje"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Estado:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=12 name="form_estado">
										<?php
											foreach ($estados_autos as $estado_auto) {
												$seleccionada = ($_POST["form_estado"] == $estado_auto["codigo"]) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $estado_auto["codigo"] ?>" <?= $seleccionada ?>><?= htmlspecialchars($estado_auto["estado"], ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Precio (sin separador de miles):</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="text" class="main_publicar_formulario_fila_campo" tabindex=13 name="form_precio" autocomplete="on" placeholder="Precio del auto" maxlength="17" value="<?= htmlspecialchars($_POST["form_precio"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Moneda:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=14 name="form_moneda">
										<?php
											foreach ($monedas as $moneda) {
												$seleccionada = ($_POST["form_moneda"] == $moneda["codigo"]) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $moneda["codigo"] ?>" <?= $seleccionada ?>><?= htmlspecialchars($moneda["moneda"], ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">(*) Destacar:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<div class="main_publicar_formulario_fila_campo_borde_select">
									<select class="main_publicar_formulario_fila_campo" tabindex=15 name="form_destacar">
										<?php
											foreach ($si_no as $codigo_si_no => $texto_si_no) {
												$seleccionada = ($_POST["form_destacar"] == $codigo_si_no) ? "selected" : "";
										?>
												<option class="main_publicar_formulario_fila_campo" value="<?= $codigo_si_no ?>" <?= $seleccionada ?>><?= htmlspecialchars($texto_si_no, ENT_COMPAT, _SITIO_CHARSET) ?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">Foto 1:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="file" class="main_publicar_formulario_fila_campo_file" tabindex=16 name="form_foto_1" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">Foto 2:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="file" class="main_publicar_formulario_fila_campo_file" tabindex=17 name="form_foto_2" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">Foto 3:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="file" class="main_publicar_formulario_fila_campo_file" tabindex=18 name="form_foto_3" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila">
							<div class="main_publicar_formulario_fila_nombre_campo">
								<p class="main_publicar_formulario_fila_nombre_campo">
									<span class="main_publicar_formulario_fila_nombre_campo">Foto 4:</span>
								</p>
							</div>
							<div class="main_publicar_formulario_fila_campo">
								<p>
									<input type="file" class="main_publicar_formulario_fila_campo_file" tabindex=19 name="form_foto_4" />
								</p>
							</div>
						</div>
						<div class="main_publicar_formulario_fila_submit">
							<input type="submit" class="main_publicar_formulario_fila_campo_submit" tabindex=20 value="Publicar" />
						</div>
					</form>
				</div>
			</div>