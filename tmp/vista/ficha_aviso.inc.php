			<!-- Á -->
			<div class="main">
				<div class="main_ficha_titulo_principal">
					<p class="main_ficha_titulo_principal">
						<span class="main_ficha_titulo_principal"><?= htmlspecialchars($aviso[0]["aviso_titulo"], ENT_COMPAT, _SITIO_CHARSET) ?></span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Fecha de publicación:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_fecha"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Usuario:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["usuario_nombre"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<?php
					if ($aviso[0]["aviso_direccion"] != "") {
				?>
						<div class="main_ficha_datos">
							<p class="main_ficha_datos">
								<span class="main_ficha_datos_campo">Dirección:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_direccion"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
							</p>
						</div>
				<?php
					}
					
					$ubicacion_geografica = "";
					if ($aviso[0]["localidad_nombre"] != "") $ubicacion_geografica .= "{$aviso[0]["localidad_nombre"]}, ";
					if ($aviso[0]["provincia_nombre"] != "") $ubicacion_geografica .= "{$aviso[0]["provincia_nombre"]}, ";
					$ubicacion_geografica .= "Argentina";
				?>
					<div class="main_ficha_datos">
						<p class="main_ficha_datos">
							<span class="main_ficha_datos_campo">Ubicación geográfica:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($ubicacion_geografica, ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
						</p>
					</div>
				<?php
					if ($aviso[0]["aviso_mostrar_mail"]) {
				?>
						<div class="main_ficha_datos">
							<p class="main_ficha_datos">
								<span class="main_ficha_datos_campo">E-mail:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["usuario_mail"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
							</p>
						</div>
				<?php
					}
					if ($aviso[0]["usuario_telefono"] != "") {
				?>
						<div class="main_ficha_datos">
							<p class="main_ficha_datos">
								<span class="main_ficha_datos_campo">Teléfono:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["usuario_telefono"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
							</p>
						</div>
				<?php
					}
					if ($aviso[0]["aviso_oferta"] == 1) {
						$oferta = "Venta";
					}
					else {
						$oferta = "Compra";
					}
				?>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Operación:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($oferta, ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Marca:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["marca_nombre"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Modelo:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["modelo_nombre"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Combustible:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["combustible_tipo"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Kilometraje:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars((double)$aviso[0]["aviso_kilometraje"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Año:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_anno_auto"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Estado:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["estado_auto"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Precio:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_precio"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Visitas:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_visitas"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<div class="main_ficha_datos">
					<p class="main_ficha_datos">
						<span class="main_ficha_datos_campo">Descripción:</span> <span class="main_ficha_datos_valor"><?= htmlspecialchars($aviso[0]["aviso_descripcion"], ENT_COMPAT, _SITIO_CHARSET) ?>.</span>
					</p>
				</div>
				<?php
					if ($aviso[0]["aviso_foto"] != "") {
				?>
						<div class="main_ficha_fotos">
							<?php
								foreach ($aviso as $foto) {
									$url_foto = _SITIO_URL_IMAGEN_AVISO . "/{$foto["aviso_codigo"]}/imagenes/{$foto["aviso_foto"]}";
							?>
									<div class="main_ficha_fotos_foto">
										<img class="" alt="" src="<?= $url_foto ?>" draggable="false" />
									</div>
							<?php
								}
							?>
						</div>
				<?php
					}
				?>
			</div>