-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: clasifiautos_db
-- ------------------------------------------------------
-- Server version	8.0.3-rc-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autos_estados`
--

DROP TABLE IF EXISTS `autos_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos_estados` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `estado` char(25) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos_estados`
--

LOCK TABLES `autos_estados` WRITE;
/*!40000 ALTER TABLE `autos_estados` DISABLE KEYS */;
INSERT INTO `autos_estados` VALUES (1,'Nuevo',1),(2,'Usado',1);
/*!40000 ALTER TABLE `autos_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autos_marcas`
--

DROP TABLE IF EXISTS `autos_marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos_marcas` (
  `codigo` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `marca` char(60) NOT NULL,
  `activa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_marca` (`marca`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos_marcas`
--

LOCK TABLES `autos_marcas` WRITE;
/*!40000 ALTER TABLE `autos_marcas` DISABLE KEYS */;
INSERT INTO `autos_marcas` VALUES (1,'Peugeot',1),(2,'Renault',1),(3,'Volkswagen',1);
/*!40000 ALTER TABLE `autos_marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autos_modelos`
--

DROP TABLE IF EXISTS `autos_modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos_modelos` (
  `codigo` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `marca` mediumint(8) unsigned NOT NULL,
  `modelo` char(60) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_marca_modelo` (`marca`,`modelo`),
  CONSTRAINT `FK_autos_modelos_marca` FOREIGN KEY (`marca`) REFERENCES `autos_marcas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos_modelos`
--

LOCK TABLES `autos_modelos` WRITE;
/*!40000 ALTER TABLE `autos_modelos` DISABLE KEYS */;
INSERT INTO `autos_modelos` VALUES (1,1,'504',1),(2,1,'505',1),(3,1,'RCZ',1),(4,2,'Clio',1),(5,2,'Kangoo',1),(6,2,'Twingo',1),(7,3,'Gol',1),(8,3,'Passat',1),(9,3,'Polo',1);
/*!40000 ALTER TABLE `autos_modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avisos`
--

DROP TABLE IF EXISTS `avisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avisos` (
  `codigo` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(10) unsigned NOT NULL,
  `fecha_hora_inicio` datetime NOT NULL,
  `duracion` smallint(5) unsigned NOT NULL DEFAULT '0',
  `direccion` char(70) DEFAULT NULL,
  `localidad` mediumint(8) unsigned NOT NULL,
  `titulo` char(200) NOT NULL,
  `descripcion` text NOT NULL,
  `mostrar_mail` tinyint(1) NOT NULL DEFAULT '1',
  `oferta` tinyint(1) NOT NULL DEFAULT '1',
  `modelo_auto` mediumint(8) unsigned NOT NULL,
  `anno_auto` smallint(5) unsigned NOT NULL,
  `combustible` smallint(5) unsigned NOT NULL,
  `kilometraje` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `estado_auto` tinyint(3) unsigned NOT NULL,
  `precio` decimal(16,2) unsigned NOT NULL,
  `moneda` smallint(5) unsigned NOT NULL,
  `destacar` tinyint(1) NOT NULL DEFAULT '0',
  `visitas` bigint(20) unsigned NOT NULL DEFAULT '0',
  `marcas_spam` bigint(20) unsigned NOT NULL DEFAULT '0',
  `revisado` tinyint(1) NOT NULL DEFAULT '0',
  `estado_aviso` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `FK_avisos_usuario` (`usuario`),
  KEY `FK_avisos_localidad` (`localidad`),
  KEY `FK_avisos_modelo_auto` (`modelo_auto`),
  KEY `FK_avisos_combustible` (`combustible`),
  KEY `FK_avisos_estado_auto` (`estado_auto`),
  KEY `FK_avisos_moneda` (`moneda`),
  KEY `FK_avisos_estado_aviso` (`estado_aviso`),
  CONSTRAINT `FK_avisos_combustible` FOREIGN KEY (`combustible`) REFERENCES `tipos_combustibles` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_estado_auto` FOREIGN KEY (`estado_auto`) REFERENCES `autos_estados` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_estado_aviso` FOREIGN KEY (`estado_aviso`) REFERENCES `avisos_estados` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_localidad` FOREIGN KEY (`localidad`) REFERENCES `localidades` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_modelo_auto` FOREIGN KEY (`modelo_auto`) REFERENCES `autos_modelos` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_moneda` FOREIGN KEY (`moneda`) REFERENCES `monedas` (`codigo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_avisos_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avisos`
--

LOCK TABLES `avisos` WRITE;
/*!40000 ALTER TABLE `avisos` DISABLE KEYS */;
INSERT INTO `avisos` VALUES (1,1,'2017-11-22 14:47:23',30,NULL,2,'Publicación 1','...',1,1,1,2017,1,7879.00,1,87987.00,1,0,0,0,0,1),(2,1,'2017-11-22 14:48:07',30,NULL,2,'Publicación 2','...',1,1,1,2017,1,7879.00,1,87987.00,1,0,1,0,0,1);
/*!40000 ALTER TABLE `avisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avisos_estados`
--

DROP TABLE IF EXISTS `avisos_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avisos_estados` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `estado` char(25) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avisos_estados`
--

LOCK TABLES `avisos_estados` WRITE;
/*!40000 ALTER TABLE `avisos_estados` DISABLE KEYS */;
INSERT INTO `avisos_estados` VALUES (1,'Válido',1),(2,'Cerrado',1),(3,'Eliminado por inválido',1);
/*!40000 ALTER TABLE `avisos_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avisos_fotos`
--

DROP TABLE IF EXISTS `avisos_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avisos_fotos` (
  `codigo` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `aviso` bigint(20) unsigned NOT NULL,
  `foto` char(10) NOT NULL,
  `miniatura` char(10) NOT NULL,
  `orden` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_aviso_foto` (`aviso`,`foto`),
  UNIQUE KEY `i_aviso_miniatura` (`aviso`,`miniatura`),
  UNIQUE KEY `i_aviso_orden` (`aviso`,`orden`),
  CONSTRAINT `FK_avisos_fotos_aviso` FOREIGN KEY (`aviso`) REFERENCES `avisos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avisos_fotos`
--

LOCK TABLES `avisos_fotos` WRITE;
/*!40000 ALTER TABLE `avisos_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `avisos_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidades`
--

DROP TABLE IF EXISTS `localidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidades` (
  `codigo` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `provincia` smallint(5) unsigned NOT NULL,
  `localidad` char(50) NOT NULL,
  `activa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_provincia_localidad` (`provincia`,`localidad`),
  CONSTRAINT `FK_localidades_provincia` FOREIGN KEY (`provincia`) REFERENCES `provincias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidades`
--

LOCK TABLES `localidades` WRITE;
/*!40000 ALTER TABLE `localidades` DISABLE KEYS */;
INSERT INTO `localidades` VALUES (1,2,'Florencio Varela',1),(2,2,'Médanos',1),(3,2,'Miramar',1),(4,3,'Boedo',1),(5,3,'Caballito',1),(6,3,'Retiro',1),(7,4,'Ancasti',1),(8,4,'Belén',1),(9,4,'San Fernando del Valle de Catamarca',1),(10,5,'Las Breñas',1),(11,5,'Quitilipi',1),(12,5,'Resistencia',1),(13,6,'Comodoro Rivadavia',1),(14,6,'Rawson',1),(15,6,'Trelew',1),(16,7,'Córdoba',1),(17,7,'Cosquín',1),(18,7,'Villa María',1),(19,8,'Corrientes',1),(20,8,'Esquina',1),(21,8,'Goya',1),(22,9,'Diamante',1),(23,9,'Gualeguaychú',1),(24,9,'Paraná',1),(25,10,'Clorinda',1),(26,10,'Comandante Fontana',1),(27,10,'Formosa',1),(28,11,'La Quiaca',1),(29,11,'San Salvador de Jujuy',1),(30,11,'Tilcara',1),(31,12,'General Pico',1),(32,12,'Realicó',1),(33,12,'Santa Rosa',1),(34,13,'Chamical',1),(35,13,'Chilecito',1),(36,13,'La Rioja',1),(37,14,'Godoy Cruz',1),(38,14,'Las Heras',1),(39,14,'Mendoza',1),(40,15,'Apóstoles',1),(41,15,'Eldorado',1),(42,15,'Posadas',1),(43,16,'Aluminé',1),(44,16,'Neuquén',1),(45,16,'San Martín de los Andes',1),(46,17,'Bariloche',1),(47,17,'San Antonio Oeste',1),(48,17,'Viedma',1),(49,18,'Rosario de la Frontera',1),(50,18,'Salta',1),(51,18,'Salvador Mazza',1),(52,19,'Caucete',1),(53,19,'Rodeo',1),(54,19,'San Juan',1),(55,20,'Arizona',1),(56,20,'Potrero de los Funes',1),(57,20,'San Luis',1),(58,21,'Caleta Olivia',1),(59,21,'El Calafate',1),(60,21,'Río Gallegos',1),(61,22,'Ceres',1),(62,22,'Rosario',1),(63,22,'Santa Fe',1),(64,23,'La Banda',1),(65,23,'Monte Quemado',1),(66,23,'Santiago del Estero',1),(67,24,'Río Grande',1),(68,24,'Tolhuin',1),(69,24,'Ushuaia',1),(70,25,'Acheral',1),(71,25,'San Miguel de Tucumán',1),(72,25,'Taco Ralo',1),(73,1,'',1),(74,2,'',1),(75,3,'',1),(76,4,'',1),(77,5,'',1),(78,6,'',1),(79,7,'',1),(80,8,'',1),(81,9,'',1),(82,10,'',1),(83,11,'',1),(84,12,'',1),(85,13,'',1),(86,14,'',1),(87,15,'',1),(88,16,'',1),(89,17,'',1),(90,18,'',1),(91,19,'',1),(92,20,'',1),(93,21,'',1),(94,22,'',1),(95,23,'',1),(96,24,'',1),(97,25,'',1);
/*!40000 ALTER TABLE `localidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monedas`
--

DROP TABLE IF EXISTS `monedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monedas` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `moneda` char(30) NOT NULL,
  `iso` char(3) NOT NULL,
  `plantilla` char(20) NOT NULL,
  `orden` smallint(5) unsigned NOT NULL,
  `activa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_iso` (`iso`),
  UNIQUE KEY `i_plantilla` (`plantilla`),
  UNIQUE KEY `i_orden` (`orden`),
  UNIQUE KEY `i_moneda` (`moneda`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monedas`
--

LOCK TABLES `monedas` WRITE;
/*!40000 ALTER TABLE `monedas` DISABLE KEYS */;
INSERT INTO `monedas` VALUES (1,'Pesos argentinos','ARS','$[v]',1,1),(2,'Dólares estadounidenses','USD','U$S [v]',2,1),(3,'Euros','EUR','[v]€',3,1);
/*!40000 ALTER TABLE `monedas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `provincia` char(50) NOT NULL,
  `activa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_provincia` (`provincia`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,'',1),(2,'Buenos Aires',1),(3,'Capital Federal',1),(4,'Catamarca',1),(5,'Chaco',1),(6,'Chubut',1),(7,'Córdoba',1),(8,'Corrientes',1),(9,'Entre Ríos',1),(10,'Formosa',1),(11,'Jujuy',1),(12,'La Pampa',1),(13,'La Rioja',1),(14,'Mendoza',1),(15,'Misiones',1),(16,'Neuquén',1),(17,'Río Negro',1),(18,'Salta',1),(19,'San Juan',1),(20,'San Luis',1),(21,'Santa Cruz',1),(22,'Santa Fe',1),(23,'Santiago del Estero',1),(24,'Tierra del Fuego',1),(25,'Tucumán',1);
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_combustibles`
--

DROP TABLE IF EXISTS `tipos_combustibles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_combustibles` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` char(40) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_tipo` (`tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_combustibles`
--

LOCK TABLES `tipos_combustibles` WRITE;
/*!40000 ALTER TABLE `tipos_combustibles` DISABLE KEYS */;
INSERT INTO `tipos_combustibles` VALUES (1,'Biocombustible',1),(2,'Diesel',1),(3,'Eléctrico',1),(4,'GNC',1),(5,'Híbrido',1),(6,'Nafta',1);
/*!40000 ALTER TABLE `tipos_combustibles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_personas`
--

DROP TABLE IF EXISTS `tipos_personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_personas` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` char(25) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_tipo` (`tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_personas`
--

LOCK TABLES `tipos_personas` WRITE;
/*!40000 ALTER TABLE `tipos_personas` DISABLE KEYS */;
INSERT INTO `tipos_personas` VALUES (1,'Concesionario',1),(2,'Particular',1);
/*!40000 ALTER TABLE `tipos_personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` char(50) NOT NULL,
  `contrasenna` char(50) NOT NULL,
  `mail` char(255) NOT NULL,
  `telefono` char(40) DEFAULT NULL,
  `tipo_persona` tinyint(3) unsigned NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `i_usuario` (`usuario`),
  UNIQUE KEY `i_mail` (`mail`),
  KEY `FK_usuarios_tipo_persona` (`tipo_persona`),
  CONSTRAINT `FK_usuarios_tipo_persona` FOREIGN KEY (`tipo_persona`) REFERENCES `tipos_personas` (`codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'user','1234','dev@127.0.0.1','+541144445555',2,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-22 12:56:26
