<?php
	//Á
	include_once("configuracion.inc.php");
	include_once("funciones.inc.php");
	
	//Establecimiento del charset, para funciones multibyte:
	@mb_internal_encoding(_SITIO_CHARSET);
	
	//Constantes relacionadas con errores de MySQL:
	define("_SITIO_ERR_MYSQL", -1);
	
	//Constantes de límites y medidas:
	define("_SITIO_MAX_FILE_SIZE", 1048576);
	define("_SITIO_MINIATURA_MAX_ANCHO", 85);
	define("_SITIO_MINIATURA_MAX_ALTO", 85);
	define("_SITIO_FOTO_MAX_ANCHO", 450);
	define("_SITIO_FOTO_MAX_ALTO", 337);
	define("_SITIO_MAX_CANT_AVISOS_PAGINA", 15);
	define("_SITIO_MAX_LONG_DESCRIPCION_EN_LISTADO", 270);
	
	//Constantes relacionadas con la inserción de usuarios:
	define("_SITIO_ERR_USUARIOS_USUARIO_SIN_ERROR", 0);
	define("_SITIO_ERR_USUARIOS_USUARIO_VACIO", -2);
	define("_SITIO_ERR_USUARIOS_USUARIO_MUY_LARGO", -3);
	define("_SITIO_ERR_USUARIOS_CONTRASENNA_VACIA", -4);
	define("_SITIO_ERR_USUARIOS_CONTRASENNA_MUY_LARGA", -5);
	define("_SITIO_ERR_USUARIOS_CONTRASENNAS_NO_COINCIDEN", -6);
	define("_SITIO_ERR_USUARIOS_MAIL_VACIO", -7);
	define("_SITIO_ERR_USUARIOS_MAIL_MUY_LARGO", -8);
	define("_SITIO_ERR_USUARIOS_MAIL_INVALIDO", -9);
	define("_SITIO_ERR_USUARIOS_TELEFONO_MUY_LARGO", -10);
	define("_SITIO_ERR_USUARIOS_TELEFONO_INVALIDO", -11);
	define("_SITIO_ERR_USUARIOS_USUARIO_REPETIDO", -12);
	
	//Constantes relacionadas con la recuperación de claves:
	define("_SITIO_ERR_RECUPERAR_CLAVE_MAIL_VACIO", -2);
	define("_SITIO_ERR_RECUPERAR_CLAVE_MAIL_MUY_LARGO", -3);
	define("_SITIO_ERR_RECUPERAR_CLAVE_MAIL_INVALIDO", -4);
	define("_SITIO_ERR_RECUPERAR_CLAVE_USUARIO_INEXISTENTE", -5);
	
	//Constantes relacionadas con el login:
	define("_SITIO_ERR_INGRESAR_USUARIO_VACIO", -2);
	define("_SITIO_ERR_INGRESAR_USUARIO_MUY_LARGO", -3);
	define("_SITIO_ERR_INGRESAR_CONTRASENNA_VACIA", -4);
	define("_SITIO_ERR_INGRESAR_CONTRASENNA_MUY_LARGA", -5);
	define("_SITIO_ERR_INGRESAR_USUARIO_CONTRASENNA_INVALIDOS", -6);
	
	//Constantes relacionadas con la publicación de avisos:
	define("_SITIO_ERR_PUBLICAR_TITULO_VACIO", -2);
	define("_SITIO_ERR_PUBLICAR_TITULO_MUY_LARGO", -3);
	define("_SITIO_ERR_PUBLICAR_DURACION_INVALIDA", -4);
	define("_SITIO_ERR_PUBLICAR_DIRECCION_MUY_LARGO", -5);
	define("_SITIO_ERR_PUBLICAR_DESCRIPCION_VACIA", -6);
	define("_SITIO_ERR_PUBLICAR_DESCRIPCION_MUY_LARGA", -7);
	define("_SITIO_ERR_PUBLICAR_ANNO_INVALIDO", -8);
	define("_SITIO_ERR_PUBLICAR_KILOMETRAJE_VACIO", -9);
	define("_SITIO_ERR_PUBLICAR_KILOMETRAJE_MUY_LARGO", -10);
	define("_SITIO_ERR_PUBLICAR_KILOMETRAJE_INVALIDO", -11);
	define("_SITIO_ERR_PUBLICAR_PRECIO_VACIO", -12);
	define("_SITIO_ERR_PUBLICAR_PRECIO_MUY_LARGO", -13);
	define("_SITIO_ERR_PUBLICAR_PRECIO_INVALIDO", -14);
	define("_SITIO_ERR_PUBLICAR_FOTO_EXTENSION_INVALIDA", -15);
	define("_SITIO_ERR_PUBLICAR_FOTO_TAMANNO_EXCESIVO", -16);
	define("_SITIO_ERR_PUBLICAR_FOTO_ARCHIVO_CORRUPTO", -17);
	define("_SITIO_ERR_PUBLICAR_FOTO_ERROR_INTERNO", -18);
	
	//Sesión:
	//session_set_cookie_params(3600, "/", _SITIO_DOMINIO_COOKIES_SITIO);	//1 hora.
	session_start();
	
	if (!isset($_SESSION["usuario_logeado"])) $_SESSION["usuario_logeado"] = "";
	if (!isset($_SESSION["pagina_actual"])) $_SESSION["pagina_actual"] = "";

	if ($_SERVER["REQUEST_URI"] != "/ingresar") {
		$_SESSION["pagina_actual"] = $_SERVER["REQUEST_URI"];
	}
	else {
		$_SESSION["pagina_actual"] = "/";
	}
	
	//Conexión con MySQL:
	$mysql_conexion = mysqli_connect(_SITIO_MYSQL_CONEXION_HOST, _SITIO_MYSQL_CONEXION_USUARIO, _SITIO_MYSQL_CONEXION_CONTRASENNA, _SITIO_MYSQL_CONEXION_BD, _SITIO_MYSQL_CONEXION_PUERTO);
	mysql_sql($mysql_conexion, "SET NAMES " . _SITIO_MYSQL_CONEXION_CHARSET . " COLLATE " . _SITIO_MYSQL_CONEXION_COLLATE);
	@mysqli_set_charset($mysql_conexion, _SITIO_MYSQL_CONEXION_CHARSET);
?>