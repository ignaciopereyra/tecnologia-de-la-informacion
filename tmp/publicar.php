<?php
	//Á
	include_once("general.inc.php");
	
	if ($_SESSION["usuario_logeado"] == "") {
		header("Location: " . _SITIO_URL_INGRESAR);
		die();
	}
	
	$meta_description = "Clasifiautos - Clasificados gratuitos de autos - Publicar";
	$meta_keywords = "Clasificados gratis, autos nuevos y usados";
	$meta_robots = "all";
	$title = "Clasifiautos - Clasificados gratuitos de autos - Publicar.";
	
	//Mensajes de error:
	$mensajes_error = array (
		_SITIO_ERR_MYSQL => "Error interno de MySQL",
		_SITIO_ERR_PUBLICAR_TITULO_VACIO => "No se ingresó el título",
		_SITIO_ERR_PUBLICAR_TITULO_MUY_LARGO => "El título no puede poseer más de 200 caracteres",
		_SITIO_ERR_PUBLICAR_DURACION_INVALIDA => "La duración es inválida",
		_SITIO_ERR_PUBLICAR_DIRECCION_MUY_LARGO => "La dirección no puede poseer más de 70 caracteres",
		_SITIO_ERR_PUBLICAR_DESCRIPCION_VACIA => "No se ingresó la descripción",
		_SITIO_ERR_PUBLICAR_DESCRIPCION_MUY_LARGA => "La descripción no puede poseer más de 65535 caracteres",
		_SITIO_ERR_PUBLICAR_ANNO_INVALIDO => "El año es inválido",
		_SITIO_ERR_PUBLICAR_KILOMETRAJE_VACIO => "No se ingresó el kilometraje",
		_SITIO_ERR_PUBLICAR_KILOMETRAJE_MUY_LARGO => "El kilometraje no puede poseer más de 13 caracteres",
		_SITIO_ERR_PUBLICAR_KILOMETRAJE_INVALIDO => "El kilometraje es inválido",
		_SITIO_ERR_PUBLICAR_PRECIO_VACIO => "No se ingresó el precio",
		_SITIO_ERR_PUBLICAR_PRECIO_MUY_LARGO => "El precio no puede poseer más de 17 caracteres",
		_SITIO_ERR_PUBLICAR_PRECIO_INVALIDO => "El precio es inválido",
		_SITIO_ERR_PUBLICAR_FOTO_EXTENSION_INVALIDA => "Las extensiones de uno o más archivos, son inválidas",
		_SITIO_ERR_PUBLICAR_FOTO_TAMANNO_EXCESIVO => "Los tamaños de uno o más archivos, son excesivos",
		_SITIO_ERR_PUBLICAR_FOTO_ARCHIVO_CORRUPTO => "Uno o más archivos, se encuentran corruptos",
		_SITIO_ERR_PUBLICAR_FOTO_ERROR_INTERNO => "Ocurrió un error interno, con uno o más de los archivos subidos"
	);
	
	$opciones_duracion = array (
		30 => "30 días",
		60 => "60 días",
		90 => "90 días",
		0 => "Infinita"
	);
	
	$localidades = mysql_select($mysql_conexion, "SELECT localidades.codigo,provincias.provincia,localidades.localidad
													FROM provincias
														LEFT OUTER JOIN localidades ON provincias.codigo=localidades.provincia
													WHERE provincias.activa=1 and localidades.activa=1
													ORDER BY provincias.provincia ASC,localidades.localidad ASC");
	
	$si_no = array (
		1 => "Sí",
		0 => "No"
	);
	
	$ofertas = array (
		1 => "Vendo",
		0 => "Compro"
	);
	
	$modelos_autos = mysql_select($mysql_conexion, "SELECT autos_modelos.codigo,autos_marcas.marca,autos_modelos.modelo 
													FROM autos_marcas 
														INNER JOIN autos_modelos on autos_marcas.codigo=autos_modelos.marca 
													WHERE autos_marcas.activa=1 
														AND autos_modelos.activo=1 
													ORDER BY autos_marcas.marca ASC,autos_modelos.modelo ASC");
	
	$annos_autos = array();
	$anno_maximo = date("Y", time());
	for ($anno = $anno_maximo; $anno >= 1900; $anno--) {
		$annos_autos[] = $anno;
	}
	
	$combustibles = mysql_select($mysql_conexion, "SELECT codigo,tipo
													FROM tipos_combustibles 
													WHERE activo=1 
													ORDER BY tipo ASC");
	
	$estados_autos = mysql_select($mysql_conexion, "SELECT codigo,estado 
													FROM autos_estados 
													WHERE activo=1 
													ORDER BY estado ASC");
	
	$monedas = mysql_select($mysql_conexion, "SELECT codigo,moneda 
												FROM monedas 
												WHERE activa=1 
												ORDER BY orden ASC");
	
	$publicacion_finalizada = false;
	if (isset($_POST["form_titulo"])) {
		foreach ($_POST as $nombre_campo => $valor) {
			$_POST[$nombre_campo] = stripslashes($valor);
		}
		$errores = insertar_aviso($_POST["form_titulo"], $_POST["form_duracion"], $_POST["form_direccion"], $_POST["form_localidad"], $_POST["form_descripcion"], $_POST["form_mostrar_mail"], $_POST["form_oferta"], $_POST["form_modelo"], $_POST["form_anno"], $_POST["form_combustible"], $_POST["form_kilometraje"], $_POST["form_estado"], $_POST["form_precio"], $_POST["form_moneda"], $_POST["form_destacar"], $_FILES, $mysql_conexion);
		if (isset($errores["codigo_aviso"])) {
			$publicacion_finalizada = true;
		}
	}
	else {
		$errores = array();
		$_POST = array (
			"form_titulo" => "",
			"form_duracion" => 30,
			"form_direccion" => "",
			"form_localidad" => 0,
			"form_descripcion" => "",
			"form_mostrar_mail" => 1,
			"form_oferta" => 1,
			"form_modelo" => 0,
			"form_anno" => $anno_maximo,
			"form_combustible" => 0,
			"form_kilometraje" => "",
			"form_estado" => 0,
			"form_precio" => "",
			"form_moneda" => 0,
			"form_destacar" => 0,
			"form_foto_1" => "",
			"form_foto_2" => "",
			"form_foto_3" => "",
			"form_foto_4" => ""
		);
	}
	
	if (!$publicacion_finalizada) {
		include_once("vista/header.inc.php");
		include_once("vista/publicar.inc.php");
		include_once("vista/footer.inc.php");
	}
	else {
		$link_aviso = str_replace(array("[codigo]", "[titulo]"), array($errores["codigo_aviso"], generar_url_simplificada($errores["titulo_aviso"])), _SITIO_URL_FICHA_AVISO);
		header("Location: $link_aviso");
		die();
	}
?>