			<div class="main">
				<!-- Á -->
				<div class="main_ingresar_titulo_principal">
					<p class="main_ingresar_titulo_principal">
						<span class="main_ingresar_titulo_principal">Ingresar</span>
					</p>
				</div>
				<?php
					if (count($errores) > 0) {
				?>
						<div class="main_ingresar_errores">
				<?php
							foreach ($errores as $error) {
				?>
								<p class="main_ingresar_errores"><span class="main_ingresar_errores"><?= htmlspecialchars($mensajes_error[$error], ENT_COMPAT, _SITIO_CHARSET) ?></span></p>
				<?php
							}
				?>
						</div>
				<?php
					}
				?>
				<div class="main_ingresar_formulario">
					<form method=post>
						<div class="main_ingresar_formulario_fila">
							<div class="main_ingresar_formulario_fila_nombre_campo">
								<p class="main_ingresar_formulario_fila_nombre_campo">
									<span class="main_ingresar_formulario_fila_nombre_campo">(*) Usuario:</span>
								</p>
							</div>
							<div class="main_ingresar_formulario_fila_campo">
								<p>
									<input type="text" class="main_ingresar_formulario_fila_campo" tabindex=1 name="form_usuario" autocomplete="on" autofocus="autofocus" placeholder="Nombre de usuario" maxlength="50" value="<?= htmlspecialchars($_POST["form_usuario"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_ingresar_formulario_fila">
							<div class="main_ingresar_formulario_fila_nombre_campo">
								<p class="main_ingresar_formulario_fila_nombre_campo">
									<span class="main_ingresar_formulario_fila_nombre_campo">(*) Contraseña:</span>
								</p>
							</div>
							<div class="main_ingresar_formulario_fila_campo">
								<p>
									<input type="password" class="main_ingresar_formulario_fila_campo" tabindex=2 name="form_contrasenna" placeholder="Contraseña" maxlength="50" value="<?= htmlspecialchars($_POST["form_contrasenna"], ENT_COMPAT, _SITIO_CHARSET) ?>" />
								</p>
							</div>
						</div>
						<div class="main_ingresar_formulario_fila_submit">
							<input type="submit" class="main_ingresar_formulario_fila_campo_submit" tabindex=3 value="Ingresar" />
						</div>
					</form>
				</div>
			</div>