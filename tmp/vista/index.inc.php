			<!-- Á -->
			<div class="main">
				<div class="main_home_titulo_principal">
					<p class="main_home_titulo_principal">
						<span class="main_home_titulo_principal">Avisos publicados</span>
					</p>
				</div>
				<?php
					if (is_array($avisos) && count($avisos) > 0) {
				?>
						<div class="main_home_avisos">
							<?php
								foreach ($avisos as $aviso) {
									if ($aviso["miniatura"] != "") {
										$foto = _SITIO_URL_IMAGEN_AVISO . "/{$aviso["codigo"]}/miniaturas/{$aviso["miniatura"]}";
									}
									else {
										$foto = _SITIO_URL_IMAGEN_INEXISTENTE;
									}
									$link_aviso = str_replace(array("[codigo]", "[titulo]"), array($aviso["codigo"], generar_url_simplificada($aviso["titulo"])), _SITIO_URL_FICHA_AVISO);
							?>
									<div class="main_home_avisos_aviso<?= ($aviso["destacar"] ? " main_home_avisos_aviso_destacado" : "") ?>">
										<div class="main_home_avisos_aviso_foto">
											<a href="<?= $link_aviso ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false">
												<img class="main_home_avisos_aviso_foto" alt="" src="<?= $foto ?>" draggable="false" />
											</a>
										</div>
										<div class="main_home_avisos_aviso_textos">
											<p class="main_home_avisos_aviso_textos_titulo">
												<a class="main_home_avisos_aviso_textos_titulo" href="<?= $link_aviso ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false">
													<?= htmlspecialchars($aviso["titulo"], ENT_COMPAT, _SITIO_CHARSET) ?>
												</a>
											</p>
											<p class="main_home_avisos_aviso_textos_fecha">
												<span class="main_home_avisos_aviso_textos_fecha">
													<?= htmlspecialchars($aviso["fecha"], ENT_COMPAT, _SITIO_CHARSET) ?>
												</span>
											</p>
											<p class="main_home_avisos_aviso_textos_descripcion">
												<span class="main_home_avisos_aviso_textos_descripcion">
													<?= htmlspecialchars(acortar_texto($aviso["descripcion"], _SITIO_MAX_LONG_DESCRIPCION_EN_LISTADO), ENT_COMPAT, _SITIO_CHARSET) ?>
												</span>
											</p>
										</div>
									</div>
							<?php
								}
							?>
						</div>
						<div class="main_home_paginador">
							<p class="main_home_paginador">
								<?php
									if ($pagina > 1) {
								?>
										<a class="main_home_paginador" href="<?= str_replace("[pagina]", 1, _SITIO_URL_LISTADO_N) ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false">Primera</a>
										<a class="main_home_paginador" href="<?= str_replace("[pagina]", $pagina - 1, _SITIO_URL_LISTADO_N) ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false"><?= $pagina - 1 ?></a>
								<?php
									}
								?>
								<a class="main_home_paginador main_home_paginador_seleccionada" href="<?= str_replace("[pagina]", $pagina, _SITIO_URL_LISTADO_N) ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false"><?= $pagina ?></a>
								<?php
									if ($pagina < $total_paginas) {
								?>
										<a class="main_home_paginador" href="<?= str_replace("[pagina]", $pagina + 1, _SITIO_URL_LISTADO_N) ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false"><?= $pagina + 1 ?></a>
										<a class="main_home_paginador" href="<?= str_replace("[pagina]", $total_paginas, _SITIO_URL_LISTADO_N) ?>" hreflang="es" media="screen and (min-width:1024px)" rel="section tag" type="text/html" draggable="false">Última</a>
								<?php
									}
								?>
							</p>
						</div>
				<?php
					}
				?>
			</div>