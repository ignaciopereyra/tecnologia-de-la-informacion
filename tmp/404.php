<?php
	//Á
	include_once("general.inc.php");
	
	header("HTTP/1.1 404 Not Found");
	
	$meta_description = "Clasifiautos - Clasificados gratuitos de autos - Página inexistente";
	$meta_keywords = "Clasificados gratis, autos nuevos y usados";
	$meta_robots = "noindex,nofollow";
	$title = "Clasifiautos - Clasificados gratuitos de autos. Página inexistente.";
	
	include_once("../vista/header.inc.php");
	include_once("../vista/404.inc.php");
	include_once("../vista/footer.inc.php");
?>